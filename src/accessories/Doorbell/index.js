'use strict';

const Handlers = require('./Handlers');

module.exports = (homebridge, mqttClient) => function (log, config) {
  const Service = homebridge.hap.Service;
  const Characteristic = homebridge.hap.Characteristic;

  const {name} = config;

  const services = {};
  const handlers = Handlers(log, config, Characteristic, services, mqttClient);

  const identify = function (callback) {
    log(`Identify Doorbell [${name}]`);
    callback(null);
  };

  const getServices = function () {
    return Object.values(services);
  };

  services.informationService = new Service.AccessoryInformation();
  services.informationService
    .setCharacteristic(Characteristic.Manufacturer, 'meluko')
    .setCharacteristic(Characteristic.Model, 'DTK_Doorbell')
    .setCharacteristic(Characteristic.SerialNumber, '0000');


  services.switchService1 = new Service.StatelessProgrammableSwitch(name);
  services.switchService1
    .getCharacteristic(Characteristic.ProgrammableSwitchEvent);
  services.switchService1
    .getCharacteristic(Characteristic.ServiceLabelIndex)
    .setValue(1);

  /*
  services.doorbellService = new Service.Doorbell(name);
  services.doorbellService
    .getCharacteristic(Characteristic.ProgrammableSwitchEvent)
    .on('get', callback => callback(null, false));

  services.switchService = new Service.Switch(this.name);
  services.switchService
    .getCharacteristic(Characteristic.On)
    .on("get", handlers.getSwitchStatus)
    .on("set", handlers.setSwitchStatus);

  setTimeout(() => {
    services.doorbellService.getCharacteristic(Characteristic.ProgrammableSwitchEvent).updateValue(true);
  }, 5000);
*/
  return {
    getServices,
    identify
  }
};
