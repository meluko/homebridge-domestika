'use strict';

module.exports = (log, config, Characteristic, services, mqttClient) => {

  const getCurrentState = function (callback) {
    callback(null, true);
  };

  const getSwitchStatus = function (callback) {
    callback(null, true);
  };

  const setSwitchStatus = function (nextSwitchStatus, callback) {
    callback(null);
  };

  return {
    getCurrentState,
    getSwitchStatus,
    setSwitchStatus
  }
};
