'use strict';

const Handlers = require('./Handlers');

module.exports = (homebridge) => function (log, config){
    const Service = homebridge.hap.Service;
    const Characteristic = homebridge.hap.Characteristic;

    const {name} = config;

    const services = {};

    const handlers = Handlers(log, config, Characteristic, services);

    const identify = function (callback) {
        log(`Identify Blind [${name}]`);
        callback(null);
    };

    const getServices = function () {
        return Object.values(services);
    };

    services.informationService = new Service.AccessoryInformation();
    services.informationService
        .setCharacteristic(Characteristic.Manufacturer, 'meluko')
        .setCharacteristic(Characteristic.Model, 'DTK_Blind')
        .setCharacteristic(Characteristic.SerialNumber, '0000');

    services.blindService = new Service.WindowCovering(name);
    services.blindService.getCharacteristic(Characteristic.CurrentPosition);
    services.blindService.getCharacteristic(Characteristic.TargetPosition);

    services.blindService
        .getCharacteristic(Characteristic.CurrentPosition)
        .on('get', handlers.getCurrentPosition);

    services.blindService
        .getCharacteristic(Characteristic.PositionState)
        .on('get', handlers.getPositionState);

    services.blindService
        .getCharacteristic(Characteristic.TargetPosition)
        .on('get', handlers.getTargetPosition)
        .on('set', handlers.setTargetPosition);

    return {
        getServices,
        identify
    }
}