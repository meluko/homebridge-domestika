'use strict';

const TuyAPI = require('tuyapi')
//    {dps: '1'} state > stop | open | close
//    {dps: '2'} position > 0 to 100 %
//    {dps: '8'} direction > forward | backward


module.exports = (log, config, Characteristic, services) => {
    const state = {
        currentPosition: 0,
        targetPosition: 0,
        positionState: Characteristic.PositionState.STOPPED,
        connected: false
    };

    delete config.tuyaApiConfig.ip
    log.info('Setting up tuya blind with config', JSON.stringify(config.tuyaApiConfig))
    const device = new TuyAPI(config.tuyaApiConfig);

    const moveToPosition = nextPosition => {
        log.info(`moving to position ${nextPosition}`);
        if (nextPosition > state.currentPosition) {
            state.positionState = Characteristic.PositionState.INCREASING
        } else if (nextPosition < state.currentPosition) {
            state.positionState = Characteristic.PositionState.DECREASING
        } else {
            state.positionState = Characteristic.PositionState.STOP
        }

        // to send specific commnad use: {dps: 1, set: <'open', 'close', 'stop'>]}
        // more info at: https://developer.tuya.com/en/docs/iot/curtain-switch-template?id=Kaely2mv9hph6
        const options = {dps: 2, set: nextPosition}
        log.info(`send options ${JSON.stringify(options)}`)

        device.set(options);
        services.blindService.getCharacteristic(Characteristic.PositionState).updateValue(state.positionState);
    }

    const getCurrentPosition = function (callback) {
        device.get()
        callback(null, state.currentPosition);
    };

    const getPositionState = function (callback) {
        device.get()
        callback(null, state.positionState);
    };

    const getTargetPosition = function (callback) {
        callback(null, state.targetPosition);
    };

    const setTargetPosition = function (nextTargetPosition, callback) {
        if (!state.connected) {
            return log.error(`error: Device not connected yet`)
        }
        log.info('set Target Position', nextTargetPosition);
        state.targetPosition = nextTargetPosition
        moveToPosition(nextTargetPosition)
        callback(null)
    };

    const onData = (payload) => {
        if (!state.connected) {
            return log.error(`error: Device not connected yet`)
        }

        if (typeof payload[0] === 'string') {
            return log.error(`error: ${JSON.stringify(payload)}`)
        }
        log.info('onData', payload);
        const {dps: {'1': positionState, '2': currentPosition, '8': direction}} = payload

        state.currentPosition = currentPosition
        state.positionState = Characteristic.PositionState.STOPPED

        services.blindService.getCharacteristic(Characteristic.CurrentPosition).updateValue(state.currentPosition);
        services.blindService.getCharacteristic(Characteristic.TargetPosition).updateValue(state.currentPosition);
        services.blindService.getCharacteristic(Characteristic.PositionState).updateValue(state.positionState);
    };

    device.on('error', error => log.info('Error!', error));
    device.on('data', onData)
    device.on('connected', () => {
        log.info('Device connected');
        state.connected = true;
    })
    device.find().then(() => device.connect())

    return {
        getCurrentPosition,
        getPositionState,
        getTargetPosition,
        setTargetPosition
    }
};
