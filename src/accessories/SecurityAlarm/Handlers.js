'use strict';

const Mapper = require('../../lib/Mapper');
const Mappings = require('./Mappings');

const constants = require('../constants');

module.exports = (log, config, Characteristic, services, mqttClient) => {
  const SecuritySystemCurrentState = Characteristic.SecuritySystemCurrentState;
  const SecuritySystemTargetState = Characteristic.SecuritySystemTargetState;

  const {installationId} = config;

  const mappings = Mappings(Characteristic);

  const translateClientStateToHomekitCurrentState = value => {
    const retValue = Mapper(mappings.CLIENT_STATE_TO_HOMEKIT_CURRENT_STATE_MAPPING)(value);
    log.info({value});
    log.info({retValue});
    return retValue
  };
  const translateClientStateToHomekitTargetState = Mapper(mappings.CLIENT_STATE_TO_HOMEKIT_TARGET_STATE_MAPPING);
  const translateHomekitTargetStateToClientState = Mapper(mappings.HOMEKIT_TARGET_STATE_TO_CLIENT_STATE_MAPPING);
  const translateHomekitStateToHumanState = Mapper(mappings.HOMEKIT_STATE_TO_HUMAN_STATE_MAPPING);

  const state = {
    securitySystemCurrentState: SecuritySystemCurrentState.DISARMED,
    securitySystemTargetState: SecuritySystemTargetState.DISARM
  };

  const getSecuritySystemCurrentState = function (callback) {
    log.info('getSecuritySystemCurrentState');
    mqttClient.publish(`alarm/${installationId}/request-confession`);
    setTimeout(() => callback(null, state.securitySystemCurrentState), constants.MQTT_GET_TIMEOUT);
  };

  const getSecuritySystemTargetState = function (callback) {
    log.info('getSecuritySystemTargetState', translateHomekitStateToHumanState(state.securitySystemTargetState));
    callback(null, state.securitySystemTargetState)
  };

  const setSecuritySystemTargetState = function (nextSecuritySystemTargetState, callback) {
    log.info('setSecuritySystemTargetState', nextSecuritySystemTargetState);
    state.securitySystemTargetState = nextSecuritySystemTargetState;
    const clientState = translateHomekitTargetStateToClientState(nextSecuritySystemTargetState);
    mqttClient.publish(`alarm/${installationId}/status/update`, {status: clientState});

    callback(null);
  };

  const handleSecuritySystemCurrentStateConfess = function (topic, status) {

    state.securitySystemCurrentState = translateClientStateToHomekitCurrentState(status);
    state.securitySystemTargetState = translateClientStateToHomekitTargetState(status);

    services.securitySystem.getCharacteristic(SecuritySystemCurrentState).updateValue(state.securitySystemCurrentState);
    services.securitySystem.getCharacteristic(SecuritySystemTargetState).updateValue(state.securitySystemTargetState);
  };

  mqttClient.subscribe(`alarm/${installationId}/status/confess`, handleSecuritySystemCurrentStateConfess);

  return {
    getSecuritySystemCurrentState,
    getSecuritySystemTargetState,
    setSecuritySystemTargetState
  }
};

