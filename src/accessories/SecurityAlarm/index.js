'use strcit';

const Handlers =  require('./Handlers');

module.exports = (homebridge, mqttClient) => function (log, config) {
  const Service = homebridge.hap.Service;
  const Characteristic = homebridge.hap.Characteristic;

  const services = {};
  const handlers = Handlers(log, config, Characteristic, services, mqttClient);

  services.informationService = new Service.AccessoryInformation();
  services.informationService
    .setCharacteristic(Characteristic.Manufacturer, 'meluko')
    .setCharacteristic(Characteristic.Model, 'DTK_Alarm')
    .setCharacteristic(Characteristic.SerialNumber, '0000');

  services.securitySystem = new Service.SecuritySystem(config.name);
  services.securitySystem
    .getCharacteristic(Characteristic.SecuritySystemCurrentState)
    .on('get', handlers.getSecuritySystemCurrentState);
  services.securitySystem
    .getCharacteristic(Characteristic.SecuritySystemTargetState)
    .setProps({validValues: [0, 1, 2, 3, 4]})
    .on('get', handlers.getSecuritySystemTargetState)
    .on('set', handlers.setSecuritySystemTargetState);


  const identify = function (callback) {
    log(`Identify Alarm [${name}]`);
    callback(null);
  };

  const getServices = function () {
    return Object.values(services);
  };

  return {
    getServices,
    identify
  }
};
