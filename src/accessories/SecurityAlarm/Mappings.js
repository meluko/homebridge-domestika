'use strict';


module.exports = Characteristic => {
  const CLIENT_STATE_TO_HOMEKIT_CURRENT_STATE_MAPPING = {
    '0': Characteristic.SecuritySystemCurrentState.STAY_ARM,
    '1': Characteristic.SecuritySystemCurrentState.AWAY_ARM,
    '2': Characteristic.SecuritySystemCurrentState.NIGHT_ARM,
    '3': Characteristic.SecuritySystemCurrentState.DISARMED,
    '4': Characteristic.SecuritySystemCurrentState.ALARM_TRIGGERED,
    default: Characteristic.SecuritySystemCurrentState.DISARMED,
  };

  const CLIENT_STATE_TO_HOMEKIT_TARGET_STATE_MAPPING = {
    '0': Characteristic.SecuritySystemTargetState.STAY_ARM,
    '1': Characteristic.SecuritySystemTargetState.AWAY_ARM,
    '2': Characteristic.SecuritySystemTargetState.NIGHT_ARM,
    '3': Characteristic.SecuritySystemTargetState.DISARM,
    '4': Characteristic.SecuritySystemTargetState.ALARM_TRIGGERED,
    default: Characteristic.SecuritySystemTargetState.DISARMED,
  };

  const HOMEKIT_TARGET_STATE_TO_CLIENT_STATE_MAPPING = {
    [Characteristic.SecuritySystemTargetState.STAY_ARM]: 0,
    [Characteristic.SecuritySystemTargetState.AWAY_ARM]: 1,
    [Characteristic.SecuritySystemTargetState.NIGHT_ARM]: 2,
    [Characteristic.SecuritySystemTargetState.DISARM]: 3,
    [Characteristic.SecuritySystemTargetState.ALARM_TRIGGERED]: 4,
    default: 3
  };

  const HOMEKIT_STATE_TO_HUMAN_STATE_MAPPING = {
    0: 'STAY_ARM',
    1: 'AWAY_ARM',
    2: 'NIGHT_ARM',
    3: 'DISARMED',
    4: 'ALARM_TRIGGERED',
    default: 'DISARMED'
  };

  return {
    CLIENT_STATE_TO_HOMEKIT_CURRENT_STATE_MAPPING,
    CLIENT_STATE_TO_HOMEKIT_TARGET_STATE_MAPPING,
    HOMEKIT_TARGET_STATE_TO_CLIENT_STATE_MAPPING,
    HOMEKIT_STATE_TO_HUMAN_STATE_MAPPING
  }
};
