'use strict';

const Handlers = require('./Handlers');

module.exports = (homebridge, mqttClient) => function (log, config) {
  const Service = homebridge.hap.Service;
  const Characteristic = homebridge.hap.Characteristic;
  const {name, presetId} = config;

  const services = {};
  const handlers = Handlers(log, config, Characteristic, services, mqttClient);

  const identify = function (callback) {
    log(`Identify Lightbulb [${presetId}]`);
    callback(null);
  };

  const getServices = function () {
    services.informationService = new Service.AccessoryInformation();
    services.informationService
      .setCharacteristic(Characteristic.Manufacturer, 'meluko')
      .setCharacteristic(Characteristic.Model, 'DTK_Relay')
      .setCharacteristic(Characteristic.SerialNumber, '0000');

    services.lightbulbService = new Service.Lightbulb(name);
    services.lightbulbService.getCharacteristic(Characteristic.On).on('get', handlers.getActive);
    services.lightbulbService.getCharacteristic(Characteristic.On).on('set', handlers.setActive);

    return Object.values(services);
  };

  mqttClient.subscribe(`presets/+/status/confess`, handlers.onStatusConfessed);
  mqttClient.publish(`presets/${presetId}/request-confession`, '');

  return {
    getServices,
    identify
  }
};
