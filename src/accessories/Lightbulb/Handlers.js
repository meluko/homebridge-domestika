'use strict';

const constants = require('../constants');

module.exports = (log, config, Characteristic, services, mqttClient) => {
  const state = {
    active: false
  };

  const getActive = callback => {
    log.info('getActive', config.presetId);
    mqttClient.publish(`presets/${config.presetId}/request-confession`, '');
    setTimeout(() => {
      log.info('getActive timed out. active:', state.active);
      callback(null, state.active)
    }, constants.MQTT_GET_TIMEOUT)
  };

  const setActive = (newActive, callback) => {
    log.info('setActive', config.presetId, newActive);
    state.active = newActive;
    const action = state.active ? 'POWER_SWITCH' : 'POWER_OFF';
    log.info(`Publising action ${action} for preset ${config.presetId}`);
    log.info(`Topic: presets/${config.presetId}/action/${action}`);
    mqttClient.publish(`presets/${config.presetId}/action/${action}`, '');

    callback(null);
  };

  const onStatusConfessed = (topic, payload, presetId) => {
    log.info('onStatusConfessed. Preset:', presetId, 'active:', payload.active);
    state.active = !!(payload && payload.active);
    services.lightbulbService.updateCharacteristic(Characteristic.On, state.active);
  };

  return {
    getActive,
    setActive,
    onStatusConfessed
  };
};
