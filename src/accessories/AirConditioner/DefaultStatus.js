'use strict';

const DEFAULT_TEMPERATURE = 21;
const DEFAULT_RELATIVE_HUMIDITY = 50;
const DEFAULT_ROTATION_SPEED = 50;

module.exports = Characteristic => {

  return {
    active: false,
    currentHeaterCoolerState: Characteristic.CurrentHeaterCoolerState.INACTIVE,
    targetHeaterCoolerState: Characteristic.TargetHeaterCoolerState.IDLE,
    currentTemperature: DEFAULT_TEMPERATURE,
    coolingThresholdTemperature: DEFAULT_TEMPERATURE,
    heatingThresholdTemperature: DEFAULT_TEMPERATURE,
    currentRelativeHumidity: DEFAULT_RELATIVE_HUMIDITY,
    rotationSpeed: DEFAULT_ROTATION_SPEED
  };

};
