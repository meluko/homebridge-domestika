'use strict';

module.exports = Characteristic => {
  const CURRENT_HEATER_COOLER_STATE_MAPPING = {
    cool: Characteristic.CurrentHeaterCoolerState.COOLING,
    heat: Characteristic.CurrentHeaterCoolerState.HEATING,
    idle: Characteristic.CurrentHeaterCoolerState.IDLE,
    inactive: Characteristic.CurrentHeaterCoolerState.INACTIVE,
    default: Characteristic.CurrentHeaterCoolerState.INACTIVE
  };

  const TARGET_HEATER_COOLER_STATE_MAPPING = {
    cool: Characteristic.TargetHeaterCoolerState.COOL,
    heat: Characteristic.TargetHeaterCoolerState.HEAT,
    auto: Characteristic.TargetHeaterCoolerState.AUTO,
    default: Characteristic.TargetHeaterCoolerState.AUTO
  };

  const CLIENT_TARGET_HEATER_COOLER_STATE_MAPPING = {
    [Characteristic.TargetHeaterCoolerState.COOL]: 'cool',
    [Characteristic.TargetHeaterCoolerState.HEAT]: 'heat',
    [Characteristic.TargetHeaterCoolerState.AUTO]: 'auto',
    default: 'auto'
  };

  const CLIENT_FAN_MODE_TO_FAN_SPEED_MAPPINGS = {
    'auto': 50,
    'low': 25,
    'medium': 50,
    'high': 75,
    'turbo': 100,
    'default': 0
  };

  const CLIENT_FAN_SPEED_TO_FAN_MODE_MAPPINGS = {
    25: 'low',
    50: 'medium',
    75: 'high',
    100: 'turbo',
    default: 'low'
  };

  const CLIENT_POWER_TO_ACTIVE_MAPPINGS = {
    on: true,
    off: false,
    default: false
  };

  return {
    CURRENT_HEATER_COOLER_STATE_MAPPING,
    TARGET_HEATER_COOLER_STATE_MAPPING,
    CLIENT_TARGET_HEATER_COOLER_STATE_MAPPING,
    CLIENT_FAN_MODE_TO_FAN_SPEED_MAPPINGS,
    CLIENT_FAN_SPEED_TO_FAN_MODE_MAPPINGS,
    CLIENT_POWER_TO_ACTIVE_MAPPINGS
  }
};
