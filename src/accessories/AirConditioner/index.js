const Handlers = require('./Handlers');

const MIN_TEMP_VALUE = 16;
const MAX_TEMP_VALUE = 30;
const MIN_TEMP_STEP = 1;

const MIN_HUMIDITY_VALUE = 0;
const MAX_HUMIDITY_VALUE = 100;
const MIN_HUMIDITY_STEP = 1;

const THRESHOLD_TEMPERATURE_PROPS = {
  minValue: MIN_TEMP_VALUE,
  maxValue: MAX_TEMP_VALUE,
  minStep: MIN_TEMP_STEP
};

const RELATIVE_HUMIDITY_PROPS = {
  minValue: MIN_HUMIDITY_VALUE,
  maxValue: MAX_HUMIDITY_VALUE,
  minStep: MIN_HUMIDITY_STEP
};

module.exports = (homebridge) => function (log, config) {
  const Service = homebridge.hap.Service;
  const Characteristic = homebridge.hap.Characteristic;

  const {
    name,
    deviceId
  } = config;

  const services = {};

  const handlers = Handlers(log, config, Characteristic);

  const identify = function (callback) {
    log(`Identify AirConditioner [${name}] [${deviceId}]`);
    callback(null);
  };

  const getServices = function () {
    services.informationService = new Service.AccessoryInformation();
    services.informationService
      .setCharacteristic(Characteristic.Manufacturer, 'meluko')
      .setCharacteristic(Characteristic.Model, 'DTK_AirConditioner')
      .setCharacteristic(Characteristic.SerialNumber, '0000');

    services.heaterCoolerService = new Service.HeaterCooler(name);
    services.heaterCoolerService.getCharacteristic(Characteristic.Active)
      .on('get', handlers.getActive)
      .on('set', handlers.setActive);
    services.heaterCoolerService.getCharacteristic(Characteristic.CurrentHeaterCoolerState)
      .on('get', handlers.getCurrentHeaterCoolerState);
    services.heaterCoolerService.getCharacteristic(Characteristic.TargetHeaterCoolerState)
      .on('get', handlers.getTargetHeaterCoolerState)
      .on('set', handlers.setTargetHeaterCoolerState);
    services.heaterCoolerService.getCharacteristic(Characteristic.CurrentTemperature)
      .setProps(THRESHOLD_TEMPERATURE_PROPS)
      .on('get', handlers.getCurrentTemperature);
    services.heaterCoolerService.getCharacteristic(Characteristic.CoolingThresholdTemperature)
      .setProps(THRESHOLD_TEMPERATURE_PROPS)
      .on('get', handlers.getCoolingThresholdTemperature)
      .on('set', handlers.setCoolingThresholdTemperature);
    services.heaterCoolerService.getCharacteristic(Characteristic.HeatingThresholdTemperature)
      .setProps(THRESHOLD_TEMPERATURE_PROPS)
      .on('get', handlers.getHeatingThresholdTemperature)
      .on('set', handlers.setHeatingThresholdTemperature);

    // ToDo: add remaining characteristics

    services.heaterCoolerService.getCharacteristic(Characteristic.RotationSpeed)
      .on('get', handlers.getRotationSpeed)
      .on('set', handlers.setRotationSpeed);

    services.humiditySensorService = new Service.HumiditySensor(name);
    services.humiditySensorService
      .getCharacteristic(Characteristic.CurrentRelativeHumidity)
      .setProps(RELATIVE_HUMIDITY_PROPS)
      .on('get', handlers.getCurrentRelativeHumidity);

    return Object.values(services);
  };

  return {
    getServices,
    identify
  }

};
