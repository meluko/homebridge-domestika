'use strict';

const SamsungAcClient = require('samsung-ac-client');

const Mapper = require('../../lib/Mapper');
const FunctionAdapater = require('../../lib/FunctionAdapater');
const Mappings = require('./Mappings');
const DefaultStatus = require('./DefaultStatus');

const CACHE_OPTIONS = {
  cacheFunctions: [
    {functionName: 'getStatus', ttl_ms: 30000}
  ],
  throttleFunctions: [
    {functionName: 'setAirConditionerFanMode', wait: 3000}
  ]
};

module.exports = (log, config, Characteristic) => {
  const {deviceId, token} = config;

  const samsungAcClient = SamsungAcClient({deviceId, token});

  const client = FunctionAdapater(CACHE_OPTIONS)(samsungAcClient);
  const status = DefaultStatus(Characteristic);

  const mappings = Mappings(Characteristic);
  const translateClientPowerToActive = Mapper(mappings.CLIENT_POWER_TO_ACTIVE_MAPPINGS);
  const translateCurrentClientAirConditionerMode = Mapper(mappings.CURRENT_HEATER_COOLER_STATE_MAPPING);
  const translateTargetClientAirConditionerMode = Mapper(mappings.TARGET_HEATER_COOLER_STATE_MAPPING);
  const translateTargetHeaterCoolerState = Mapper(mappings.CLIENT_TARGET_HEATER_COOLER_STATE_MAPPING);
  const translateFanModeToFanSpeed = Mapper(mappings.CLIENT_FAN_MODE_TO_FAN_SPEED_MAPPINGS);

  const getActive = function (callback) {
    log.debug('getActive');
    client
      .getStatus()
      .then(({power}) => {
        status.active = translateClientPowerToActive(power);
        callback(null, status.active);
      })
      .catch(callback);
  };

  const setActive = function (nextActive, callback) {
    log.debug('setActive:', nextActive);
    client
      .setActive(nextActive)
      .then(() => {
        status.active = nextActive;
        callback(null);
      })
      .catch(callback);
  };

  const getCurrentHeaterCoolerState = function (callback) {
    log.debug('getCurrentHeaterCoolerState');
    client
      .getStatus()
      .then(({airConditionerMode}) => {
        status.currentHeaterCoolerState = translateCurrentClientAirConditionerMode(airConditionerMode);
        callback(null, status.currentHeaterCoolerState);
      })
      .catch(callback);
  };

  const getTargetHeaterCoolerState = function (callback) {
    log.debug('getTargetHeaterCoolerState');
    client
      .getStatus()
      .then(({airConditionerMode}) => {
        status.currentHeaterCoolerState = translateTargetClientAirConditionerMode(airConditionerMode);
        callback(null, status.currentHeaterCoolerState);
      })
      .catch(callback);
  };

  const setTargetHeaterCoolerState = function (nextTargetHeaterCoolerState, callback) {
    log.debug('setTargetHeaterCoolerState:', nextTargetHeaterCoolerState);
    const clientMode = translateTargetHeaterCoolerState(nextTargetHeaterCoolerState);
    client
      .setAirConditionerMode(clientMode)
      .then(() => {
        status.targetHeaterCoolerState = nextTargetHeaterCoolerState;
        callback(null);
      })
      .catch(callback);
  };

  const getCurrentTemperature = function (callback) {
    log.debug('getCurrentTemperature');
    client
      .getStatus()
      .then(({roomTemperature}) => {
        status.currentTemperature = roomTemperature;
        callback(null, status.currentTemperature);
      })
      .catch(callback);
  };

  const getCoolingThresholdTemperature = function (callback) {
    log.debug('getCoolingThresholdTemperature');
    client
      .getStatus()
      .then(({targetTemperature}) => {
        status.coolingThresholdTemperature = targetTemperature;
        callback(null, status.coolingThresholdTemperature);
      })
      .catch(callback);
  };

  const setCoolingThresholdTemperature = function (nextCoolingThresholdTemperature, callback) {
    log.debug('setCoolingThresholdTemperature', nextCoolingThresholdTemperature);

    status.coolingThresholdTemperature = nextCoolingThresholdTemperature;
    client
      .setCoolingSetpoint(nextCoolingThresholdTemperature)
      .then(() => callback(null))
      .catch(callback);
  };

  const getHeatingThresholdTemperature = function (callback) {
    log.debug('getHeatingThresholdTemperature');
    client
      .getStatus()
      .then(({targetTemperature}) => {
        status.heatingThresholdTemperature = targetTemperature;
        callback(null, status.heatingThresholdTemperature);
      })
      .catch(callback);
  };

  const setHeatingThresholdTemperature = function (nextHeatingThresholdTemperature, callback) {
    log.debug('setHeatingThresholdTemperature', nextHeatingThresholdTemperature);

    client
      .setCoolingSetpoint(nextHeatingThresholdTemperature)
      .then(() => {
        status.heatingThresholdTemperature = nextHeatingThresholdTemperature;
        callback(null)
      })
      .catch(callback);
  };

  const matchNearest = (object, valueToFind, defaultLowestValue = 0) => {
    const entries = Object
      .entries(object)
      .map(([value, data]) => ({value, data}));
    let lowestValue = defaultLowestValue;

    return entries.find(({value}) => {
      const matches = value >= valueToFind && valueToFind >= lowestValue;
      lowestValue = value;
      return matches;
    }).data;
  };

  const getRotationSpeed = function (callback) {
    log.debug('getRotationSpeed');
    client
      .getStatus()
      .then(({fanMode}) => {
        status.rotationSpeed = translateFanModeToFanSpeed(fanMode);
        callback(null, status.rotationSpeed);
      })
      .catch(callback);
  };

  const setRotationSpeed = function (nextRotationSpeed, callback) {
    log.debug('setRotationSpeed');
    const nextClientFanMode = matchNearest(mappings.CLIENT_FAN_SPEED_TO_FAN_MODE_MAPPINGS, nextRotationSpeed);

    client
      .setAirConditionerFanMode(nextClientFanMode)
      .then(() => {
        status.rotationSpeed = nextRotationSpeed;
        callback(null);
      })
      .catch(callback);
  };

  const getCurrentRelativeHumidity = function (callback) {
    log.debug('getCurrentRelativeHumidity');
    client
      .getStatus()
      .then(({relativeHumidity}) => {
        status.currentRelativeHumidity = relativeHumidity;
        callback(null, status.currentRelativeHumidity);
      })
      .catch(callback);
  };

  return {
    getActive,
    setActive,
    getCurrentHeaterCoolerState,
    getTargetHeaterCoolerState,
    setTargetHeaterCoolerState,
    getCurrentTemperature,
    getCoolingThresholdTemperature,
    setCoolingThresholdTemperature,
    getHeatingThresholdTemperature,
    setHeatingThresholdTemperature,
    getRotationSpeed,
    setRotationSpeed,
    getCurrentRelativeHumidity
  }
}
;
