'use strict';

module.exports = {
  AirConditioner: require('./AirConditioner'),
  GarageDoor: require('./GarageDoor'),
  Lightbulb: require('./Lightbulb'),
  SecurityAlarm: require('./SecurityAlarm'),
  SmartSpeaker: require('./SmartSpeaker'),
  Doorbell: require('./Doorbell'),
  Blind: require('./Blind'),
  Fan: require('./Fan')
};
