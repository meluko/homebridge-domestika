'use strict';

const Handlers = require('./Handlers');

module.exports = (homebridge, mqttClient) => function (log, config) {
  const Service = homebridge.hap.Service;
  const Characteristic = homebridge.hap.Characteristic;

  const {name, deviceName} = config;

  const services = {};
  const handlers = Handlers(log, config, Characteristic, services, mqttClient);

  const identify = function (callback) {
    log(`Identify GarageDoor [${name}] [${deviceName}]`);
    callback(null);
  };

  const getServices = function () {
    return Object.values(services);
  };

  services.informationService = new Service.AccessoryInformation();
  services.informationService
    .setCharacteristic(Characteristic.Manufacturer, 'meluko')
    .setCharacteristic(Characteristic.Model, 'DTK_Relay')
    .setCharacteristic(Characteristic.SerialNumber, '0000');

  services.garageDoorService = new Service.GarageDoorOpener(name);
  services.garageDoorService
    .getCharacteristic(Characteristic.CurrentDoorState)
    .on('get', handlers.getCurrentDoorState)
    .on('set', handlers.setCurrentDoorState);

  services.garageDoorService
    .getCharacteristic(Characteristic.TargetDoorState)
    .on('get', handlers.getTargetDoorState)
    .on('set', handlers.setTargetDoorState);

  mqttClient.subscribe(`devices/${deviceName}/switch-sequence/+`, handlers.onSequence);
  mqttClient.subscribe(`devices/${deviceName}/status/confess`, handlers.onStatusConfessed);

  return {
    getServices,
    identify
  }
};
