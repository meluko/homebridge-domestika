'use strict';

const constants = require('../constants');

const DOOR_STATE_CLOSED = 'DOOR_STATE_CLOSED';
const DOOR_STATE_CLOSING = 'DOOR_STATE_CLOSING';
const DOOR_STATE_OPEN = 'DOOR_STATE_OPEN';
const DOOR_STATE_OPENING = 'DOOR_STATE_OPENING';

const CLIENT_STATUS_TO_DOOR_STATE_MAPPING = {
  '0C': DOOR_STATE_OPEN,
  '0O': DOOR_STATE_CLOSING,
  '1C': DOOR_STATE_CLOSED,
  '1O': DOOR_STATE_OPENING
};

module.exports = (log, config, Characteristic, services, mqttClient) => {
  const TargetDoorState = Characteristic.TargetDoorState;
  const CurrentDoorState = Characteristic.CurrentDoorState;

  const state = {
    currentDoorState: Characteristic.CurrentDoorState.OPEN,
    targetDoorState: Characteristic.TargetDoorState.OPEN
  };

  const getCurrentDoorState = function (callback) {
    log.info('getCurrentDoorState');
    mqttClient.publish(`devices/${config.deviceName}/request-confession`);
    setTimeout(() => callback(null, state.currentDoorState), constants.MQTT_GET_TIMEOUT);
  };

  const setCurrentDoorState = function (nextCurrentDoorState, callback) {
    log.info('setCurrentDoorState');
    state.currentDoorState = nextCurrentDoorState;
    callback(null);
  };

  const getTargetDoorState = function (callback) {
    log.info('getTargetDoorState');
    callback(null, state.targetDoorState)
  };

  const setTargetDoorState = function (nextTargetDoorState, callback) {
    log.info('setTargetDoorState');
    state.targetDoorState = nextTargetDoorState;
    callback(null);
  };

  const DOOR_STATES = {
    [DOOR_STATE_CLOSED]: {
      targetDoorState: TargetDoorState.CLOSED,
      currentDoorState: CurrentDoorState.CLOSED
    },
    [DOOR_STATE_CLOSING]: {
      targetDoorState: TargetDoorState.CLOSED,
      currentDoorState: CurrentDoorState.CLOSING
    },
    [DOOR_STATE_OPEN]: {
      targetDoorState: TargetDoorState.OPEN,
      currentDoorState: CurrentDoorState.OPEN
    },
    [DOOR_STATE_OPENING]: {
      targetDoorState: TargetDoorState.OPEN,
      currentDoorState: CurrentDoorState.OPENING
    }
  };

  const setDoorState = function (doorState = DOOR_STATES.closed) {
    console.log('set target to', doorState.targetDoorState);
    console.log('set current to', doorState.currentDoorState);

    state.currentDoorState = doorState.currentDoorState;
    state.targetDoorState = doorState.currentDoorState;

    services.garageDoorService.getCharacteristic(TargetDoorState).updateValue(doorState.targetDoorState);
    services.garageDoorService.getCharacteristic(CurrentDoorState).updateValue(doorState.currentDoorState);
  };

  const onSequence = function (topic, payload, sequence) {
    console.log(`Sequence received [${sequence}]`);
    const doorState = CLIENT_STATUS_TO_DOOR_STATE_MAPPING[sequence];
    setDoorState(DOOR_STATES[doorState]);
  };

  const onStatusConfessed = function (topic, payload) {
    onSequence(topic, payload, payload.lastSequence);
  };

  return {
    getCurrentDoorState,
    setCurrentDoorState,
    getTargetDoorState,
    setTargetDoorState,
    onStatusConfessed,
    onSequence
  }
};
