'use strict';

const MQTT_GET_TIMEOUT = 250;

module.exports = {
  MQTT_GET_TIMEOUT
};
