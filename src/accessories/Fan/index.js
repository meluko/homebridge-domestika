'use strict';

const Handlers = require('./Handlers');

module.exports = (homebridge, mqttClient) => function (log, config) {
  const Service = homebridge.hap.Service;
  const Characteristic = homebridge.hap.Characteristic;
  const {name, presetId} = config;

  const services = {};
  const handlers = Handlers(log, config, Characteristic, services, mqttClient);

  const identify = function (callback) {
    log(`Identify Lightbulb [${presetId}]`);
    callback(null);
  };

  const getServices = function () {
    services.informationService = new Service.AccessoryInformation();
    services.informationService
      .setCharacteristic(Characteristic.Manufacturer, 'meluko')
      .setCharacteristic(Characteristic.Model, 'DTK_Relay')
      .setCharacteristic(Characteristic.SerialNumber, '0000');

    services.fanService = new Service.Fan(name);
    services.fanService.getCharacteristic(Characteristic.On).on('get', handlers.getActive);
    services.fanService.getCharacteristic(Characteristic.On).on('set', handlers.setActive);

    services.fanService.getCharacteristic(Characteristic.RotationSpeed).on('get', handlers.getRotationSpeed);
    services.fanService.getCharacteristic(Characteristic.RotationSpeed).on('set', handlers.setRotationSpeed);

    return Object.values(services);
  };

  mqttClient.subscribe('devices/+/confess/+', handlers.onStatusConfessed)
  mqttClient.publish('devices/status/request-confession', '')

  return {
    getServices,
    identify
  }
};
