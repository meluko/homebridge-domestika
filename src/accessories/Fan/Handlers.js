'use strict';

const constants = require('../constants');

const SPEED_LEVELS = [
  {level: '0%',   value: '0',   mask: 0x00000030, presetId: 'kitchenFan0', action: 'POWER_OFF'},
  {level: '50%',  value: '50',  mask: 0x00000010, presetId: 'kitchenFan2', action: 'POWER_SWITCH'},
  {level: '100%', value: '100', mask: 0x00000020, presetId: 'kitchenFan1', action: 'POWER_SWITCH'}
]

module.exports = (log, config, Characteristic, services, mqttClient) => {

  const state = {
    active: false,
    speedLevels: SPEED_LEVELS,
    speedLevel: SPEED_LEVELS[0],
    device: 'M10_F02_D01'
    // speedLevels: config.speedLevels,
    // rotationSpeed: config.speedLevels[0]
  };

  const getActive = callback => {
    log.info('getActive', config.prespresetIdetId);
    mqttClient.publish('devices/status/request-confession', '')
    // mqttClient.publish(`presets/${config.presetId}/request-confession`, '');
    setTimeout(() => {
      log.info('getActive timed out. active:', state.active);
      callback(null, state.active)
    }, constants.MQTT_GET_TIMEOUT)
  };

  const getRotationSpeed = (callback) => {
    log.info('getRotationSpeed', config.presetId);
    mqttClient.publish('devices/status/request-confession', '')
    // mqttClient.publish(`presets/${config.presetId}/request-confession`, '');
    setTimeout(() => {
      log.info('getRotationSpeed timed out. rotationSpeed:', state.speedLevel.value);
      callback(null, state.speedLevel.value)
    }, constants.MQTT_GET_TIMEOUT)
  }

  const onStatusConfessed = (topic, payload, device, hexStatus) => {
    const status = parseInt(hexStatus, 16)
    log.info('onStatusConfessed', topic, device, status, `0x${hexStatus}`)
    if (device !== state.device) {
      log.info(`device [${device}] does not match [${state.device}]`);
      return
    }

    const speedLevel = SPEED_LEVELS.find(it => !!(it.mask & status)) || SPEED_LEVELS[0]
    state.speedLevel = speedLevel
    state.active = !!(parseInt(speedLevel.value))

    // services.fanService.updateCharacteristic(Characteristic.RotationSpeed, rotationSpeed);
    services.fanService
      .getCharacteristic(Characteristic.RotationSpeed)
      .updateValue(state.speedLevel.value, undefined, 'fromSetValue');

    // services.fanService.updateCharacteristic(Characteristic.On, active);
    services.fanService.getCharacteristic(Characteristic.On)
      .updateValue(state.active, undefined, 'fromSetValue');
  };

  const setActive = (newActive, callback) => {
    state.active = newActive
    callback(null);
  };

  const setRotationSpeed = (rotationSpeed, callback) => {
    log.info('setRotationSpeed', config.presetId, rotationSpeed);
    const [selectedSpeed] = SPEED_LEVELS
      .map(it => ({...it, speedDistance: Math.abs(parseInt(it.value) - rotationSpeed), value: parseInt(it.value)}))
      .sort((a, b) => a.speedDistance - b.speedDistance)
    state.speedLevel = selectedSpeed;
    log.info(JSON.stringify(selectedSpeed))

    // mqttClient.publish(`presets/${selectedSpeed.presetId}/action/${selectedSpeed.action}`, '');

    callback(null);
  }

  return {
    getActive,
    setActive,
    getRotationSpeed,
    setRotationSpeed,
    onStatusConfessed
  };
};
