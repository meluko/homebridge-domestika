// MQTT
'use strict';

var Service, Characteristic;
var mqtt = require("mqtt");


function mqttfanAccessory(log, config) {
  this.log          	= log;
  this.name 			= config["name"];
  this.url 			= config["url"];
  this.sn = config['sn'] || 'Unknown';
  this.model = config['model'] || 'Fan';
  this.client_Id 		= 'mqttjs_' + Math.random().toString(16).substr(2, 8);
  this.options = {
    keepalive: 10,
    clientId: this.client_Id,
    protocolId: 'MQTT',
    protocolVersion: 4,
    clean: true,
    reconnectPeriod: 1000,
    connectTimeout: 30 * 1000,
    will: {
      topic: 'WillMsg',
      payload: 'Connection Closed abnormally..!',
      qos: 0,
      retain: false
    },
    username: config["username"],
    password: config["password"],
    rejectUnauthorized: false
  };
  this.caption		= config["caption"];
  this.topics = config["topics"];
  this.on = false;
  this.rotationSpeed = 0;


  this.service = new Service.Fan(this.name);
  this.service
    .getCharacteristic(Characteristic.On)
    .on('get', this.getStatus.bind(this))
    .on('set', this.setStatus.bind(this));
  this.service
    .getCharacteristic(Characteristic.RotationSpeed)
    .on('get', this.getRotationSpeed.bind(this))
    .on('set', this.setRotationSpeed.bind(this));

  // connect to MQTT broker
  this.client = mqtt.connect(this.url, this.options);
  var that = this;
  this.client.on('error', function (err) {
    that.log('Error event on MQTT:', err);
  });

  this.client.on('message', function (topic, message) {
    // console.log(message.toString(), topic);

    if (topic == that.topics.getOn) {
      var status = message.toString();
      that.on = (status == "true" ? true : false);
      that.service.getCharacteristic(Characteristic.On).setValue(that.on, undefined, 'fromSetValue');
    }

    if (topic == that.topics.getRotationSpeed) {
      var val = parseInt(message.toString());
      that.rotationSpeed = val;
      that.service.getCharacteristic(Characteristic.RotationSpeed).setValue(that.rotationSpeed, undefined, 'fromSetValue');
    }

  });
  this.client.subscribe(this.topics.getOn);
  this.client.subscribe(this.topics.getRotationSpeed);

}

module.exports = function(homebridge) {
  Service = homebridge.hap.Service;
  Characteristic = homebridge.hap.Characteristic;

  homebridge.registerAccessory("homebridge-fan-accessory", "fan-accessory-rotation", mqttfanAccessory);
}

mqttfanAccessory.prototype.getStatus = function(callback) {
  callback(null, this.on);
}

mqttfanAccessory.prototype.setStatus = function(status, callback, context) {
  if(context !== 'fromSetValue') {
    this.on = status;
    this.client.publish(this.topics.setOn, status ? "true" : "false");
  }
  callback();
}

mqttfanAccessory.prototype.getRotationSpeed = function(callback) {
  callback(null, this.rotationSpeed);
}

mqttfanAccessory.prototype.setRotationSpeed = function(rotationSp, callback, context) {
  if(context !== 'fromSetValue') {
    this.rotationSpeed = rotationSp;
    this.client.publish(this.topics.setRotationSpeed, this.rotationSpeed.toString());
  }
  callback();
}


mqttfanAccessory.prototype.getServices = function() {
  var informationService = new Service.AccessoryInformation();

  informationService
    .setCharacteristic(Characteristic.Name, this.name)
    .setCharacteristic(Characteristic.Manufacturer, "Wayne Automation")
    .setCharacteristic(Characteristic.Model, this.model)
    .setCharacteristic(Characteristic.SerialNumber, this.sn);
  return [informationService,this.service];
}