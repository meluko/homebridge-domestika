'use strict';

const constants = require('../constants');

module.exports = (log, config, Characteristic, services, mqttClient) => {
  const {
    CurrentMediaState,
    TargetMediaState,
  } = Characteristic;
  const state = {
    mute: false,
    volume: 50,
    currentMediaState: CurrentMediaState.PAUSE,
    targetMediaState: TargetMediaState.PAUSE
  };

  const getMute = (callback) => {
    console.log('getMute', state.mute);
    callback(null, state.mute);
  };

  const setMute = (newMute, callback) => {
    console.log('setMute', newMute);
    state.mute = newMute;
    callback(null);
  };

  const getVolume = (callback) => {
    console.log('getVolume', state.volume);
    callback(null, state.volume);
  };

  const setVolume = (newVolume, callback) => {
    console.log('setVolume', newVolume);
    state.volume = newVolume;
    callback(null);
  };

  const getCurrentMediaState = (callback) => {
    console.log('getCurrentMediaState', state.currentMediaState);
    callback(null, state.currentMediaState);
  };

  const getTargetMediaState = (callback) => {
    console.log('getTargetMediaState', state.targetMediaState);
    callback(null, state.targetMediaState);
  };

  const setTargetMediaState = (newTargetMediaState, callback) => {
    console.log('setTargetMediaState', newTargetMediaState);
    state.targetMediaState = newTargetMediaState;
    callback(null);
  };

  return {
    getMute,
    setMute,
    getVolume,
    setVolume,
    getCurrentMediaState,
    getTargetMediaState,
    setTargetMediaState
  };
};
