'use strict';

const Handlers = require('./Handlers');

module.exports = (homebridge, mqttClient) => function (log, config) {
  const Service = homebridge.hap.Service;
  const Characteristic = homebridge.hap.Characteristic;
  const {name, accessoryGroup} = config;

  const services = {};
  const handlers = Handlers(log, config, Characteristic, services, mqttClient);

  const identify = function (callback) {
    log(`Identify SmartSpeaker [${name}]`);
    callback(null);
  };

  const getServices = function () {
    services.informationService = new Service.AccessoryInformation();
    services.informationService
      .setCharacteristic(Characteristic.Manufacturer, 'meluko')
      .setCharacteristic(Characteristic.Model, 'DTK_Relay')
      .setCharacteristic(Characteristic.SerialNumber, '0000');

    services.smartSpeakerService = new Service.SmartSpeaker(name);
    services.smartSpeakerService.setCharacteristic(Characteristic.Name, name);
    services.smartSpeakerService.setCharacteristic(Characteristic.Mute, true);
    services.smartSpeakerService.setCharacteristic(Characteristic.ConfiguredName, `SmarSpeaker - ${name}`);
    services.smartSpeakerService.setCharacteristic(Characteristic.Volume, 40);


    services.smartSpeakerService.getCharacteristic(Characteristic.Volume).on('get', handlers.getVolume);
    services.smartSpeakerService.getCharacteristic(Characteristic.Volume).on('set', handlers.setVolume);
    services.smartSpeakerService.getCharacteristic(Characteristic.CurrentMediaState).on('get', handlers.getCurrentMediaState);
    services.smartSpeakerService.getCharacteristic(Characteristic.TargetMediaState).on('get', handlers.getTargetMediaState);
    services.smartSpeakerService.getCharacteristic(Characteristic.TargetMediaState).on('set', handlers.setTargetMediaState);
    services.smartSpeakerService.getCharacteristic(Characteristic.Mute).on('get', handlers.getMute);
    services.smartSpeakerService.getCharacteristic(Characteristic.Mute).on('set', handlers.setMute);

    return Object.values(services);
  };

  return {
    getServices,
    identify
  }
};
