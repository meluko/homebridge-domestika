'use strict';

const PromisedMQTTClient = require('./PromisedMQTTClient');
const hbGlobalConfig = require('./hbGlobalConfig');

module.exports = (homebridge) => {
  const {domestika} = hbGlobalConfig(homebridge);
  const mqttClient = PromisedMQTTClient(domestika.mqtt);
  return accessory => accessory(homebridge, mqttClient);
};
