'use strict';

const MQTTClient = require('MQTTClient');

module.exports = params => {
  const mqttClient = MQTTClient(params);

  return {
    publish: (...args) => mqttClient.then(mqttClient => mqttClient.publish(...args)),
    subscribe: (...args) => mqttClient.then(mqttClient => mqttClient.subscribe(...args))
  }
};
