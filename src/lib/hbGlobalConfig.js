'use strict';

module.exports = homebridge => {
  return require(`${homebridge.user.customStoragePath}/config.json`);
};
