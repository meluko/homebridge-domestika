'use strict';

const _ = require('lodash');
const CACHE_TIME_MS = 30000;


module.exports = config => allFunctions => {
  let cache = {};

  const getCached = (ttl_ms = CACHE_TIME_MS) => functionName => {
    return !!cache[functionName] && Date.now() - cache[functionName].timestamp < ttl_ms && cache[functionName].value;
  };

  const setCached = (functionName, ...args) => {
    const value = allFunctions[functionName](...args);
    cache[functionName] = {
      value,
      timestamp: Date.now()
    };

    return value;
  };

  const cachedFunctions = (config.cachedFunctions || [])
    .reduce((cachedFunctions, {functionName, ttl_ms}) => {
      cachedFunctions[functionName] = (...args) => {
        let cached = getCached(ttl_ms)(functionName);
        if (cached) {
          return cached;
        }
        return setCached(functionName, ...args);
      };

      return cachedFunctions;
    }, {});

  const throttleFunctions = (config.throttleFunctions || [])
    .reduce((cachedFunctions, opts) => {
      cachedFunctions[opts.functionName] = _.throttle(allFunctions[opts.functionName], opts.wait);
      return cachedFunctions;
    }, {});

  return {
    ...allFunctions,
    ...cachedFunctions,
    ...throttleFunctions
  }
};
