'use strict';

module.exports = dictionary => {
  return value => {
    const returnValue = dictionary[value];
    return returnValue !== undefined ? returnValue : dictionary['default'];
  };
}
