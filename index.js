'use strict';

const {
    AirConditioner,
    SecurityAlarm,
    GarageDoor,
    Lightbulb,
    SmartSpeaker,
    Doorbell,
    Blind,
    Fan
} = require('./src/accessories');
const AccessoryWrapper = require('./src/lib/AccessoryWrapper');

module.exports = function (homebridge) {
    const accessoryWrapper = AccessoryWrapper(homebridge);

    homebridge.registerAccessory("homebridge-Domestika", "DomestikaAirConditioner", accessoryWrapper(AirConditioner));
    homebridge.registerAccessory("homebridge-Domestika", "DomestikaDoorbell", accessoryWrapper(Doorbell));
    homebridge.registerAccessory("homebridge-Domestika", "DomestikaGarageDoor", accessoryWrapper(GarageDoor));
    homebridge.registerAccessory("homebridge-Domestika", "DomestikaLightbulb", accessoryWrapper(Lightbulb));
    homebridge.registerAccessory("homebridge-Domestika", "DomestikaSecurityAlarm", accessoryWrapper(SecurityAlarm));
    homebridge.registerAccessory("homebridge-Domestika", "DomestikaSmartSpeaker", accessoryWrapper(SmartSpeaker));
    homebridge.registerAccessory("homebridge-Domestika", "DomestikaBlind", accessoryWrapper(Blind));
    homebridge.registerAccessory("homebridge-Domestika", "DomestikaFan", accessoryWrapper(Fan));
};
